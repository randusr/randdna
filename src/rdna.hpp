#include <iostream>
#include <random>
using namespace std;
 
string randDNA (int seed, string bases, int n)
{ 
	std::mt19937 eng1(seed); 
	int min = 0; 
	int max = bases.size()-1; 

	if (bases == "")
	{ 
		return ""; 
	} 

	std::uniform_int_distribution<int> unifrm(min, max); 
	int choose = 0; 
	int i = 0; 
	string dna = ""; 

	while (i < n)
	{ 
		choose = unifrm(eng1); dna += bases[choose]; ++i; 
	} 

	return dna; 
}
